#!/bin/sh

err=0

for bin in "sap_cluster_connector help"; do
	output="$($bin 2>&1)"
	if [ $? -ne 0 ]; then
		echo -e "ERROR: $bin failed:\n$output"
		err=$((err+1))
	else
		echo "INFO: $bin works..."
	fi
done

if [ $err -ge 1 ]; then
	echo -e "\nERROR: $err tests FAILED..."
	exit 1
fi
