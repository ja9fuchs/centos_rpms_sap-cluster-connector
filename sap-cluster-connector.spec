#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# Below is the script used to generate a new source file
# from the sap_cluster_connector upstream git repo.
#
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="sap_cluster_connector-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#

%global sap_script_prefix sap_cluster_connector
%global sap_script_hash ba8c22e

Name:     sap-cluster-connector
Summary:  SAP cluster connector script
Version:  3.0.1
Release:  10%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}%{?dist}.1
License:  GPLv2+
URL:      https://github.com/redhat-sap/sap_cluster_connector
%if 0%{?fedora} || 0%{?centos_version} || 0%{?rhel}
Group:    System Environment/Base
%else
Group:    Productivity/Clustering/HA
%endif
Source0:  %{sap_script_prefix}-%{sap_script_hash}.tar.gz

BuildArch:  noarch

BuildRequires: perl-generators

Requires: resource-agents-sap >= 4.8.0
Requires: perl-interpreter

%description
The SAP connector script interface with Pacemaker to allow SAP
instances to be managed in a cluster environment.

%prep
%setup -q -n %{sap_script_prefix}-%{sap_script_hash}

%build

%install
rm -rf %{buildroot}
test -d %{buildroot}/%{_bindir} || mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_datadir}/sap_cluster_connector
mkdir -p %{buildroot}/%{_mandir}/man8
cp sap_cluster_connector %{buildroot}/%{_bindir}
cp -rv {run_checks,checks} %{buildroot}/%{_datadir}/sap_cluster_connector
gzip man/*.8
cp man/*.8.gz %{buildroot}/%{_mandir}/man8

%files
%defattr(-,root,root)
%{_bindir}/sap_cluster_connector
%{_mandir}/man8/sap_cluster_connector*
%{_datadir}/sap_cluster_connector

%changelog
* Thu Feb 20 2025 Janine Fuchs <jfuchs@redhat.com> - 3.0.1-10.1
- Fix output parser mismatch since pacemaker 2.1.6.

  Resolves: RHEL-80319

* Thu Dec 12 2024 Janine Fuchs <jfuchs@redhat.com> - 3.0.1-8
- Changed perl dependency to perl-interpreter

  Resolves: RHEL-60115

* Tue Aug 10 2021 Mohan Boddu <mboddu@redhat.com> - 3.0.1-7.1
- Rebuilt for IMA sigs, glibc 2.34, aarch64 flags
  Related: rhbz#1991688

* Tue Jun 22 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 3.0.1-7
- Initial build

  Resolves: rhbz#1960248

* Mon May 10 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 3.0.1-6
- sap-cluster-connector: allow dashes/underscores in nodenames

  Resolves: rhbz#1827096

* Tue Jun 25 2019 Oyvind Albrigtsen <oalbrigt@redhat.com> - 3.0.1-4
- Initial build as separate package

  Resolves: rhbz#1688346

# vim:set ai ts=2 sw=2 sts=2 et:
